import PackageDescription

let package = Package(
    name: "URI",
    dependencies: [
        .Package(url: "https://fengluo@bitbucket.org/fengluo/itsari.git", majorVersion: 1)
    ]
)